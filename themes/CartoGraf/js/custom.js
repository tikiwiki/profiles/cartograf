/**
 * Cartograf custom js
 */

let searchParams = new URLSearchParams(window.location.hash.substr(1)); // skip the first char (#)

if (location.hash && $) {
	// set map features from location.hash needs to be before doc ready
	let $mapContainer = $(".map-container");

	if (searchParams.has("EmbedMode")) {
		$mapContainer.data("mapControls", $mapContainer.data("mapControls").replace(",overview", ""));
		$(".sitelogo img").css("max-width", "50%");
		$(".anchor-toggle img").css("width", "70%");
		$("h3.anchor-head, h3.anchor-head > *").css({
				height:"45px",
				lineHeight: "45px",
				borderWidth: "3px"
			});
	}

	if (searchParams.has("Controls")) {
		// any of: controls, layers, search_location, levels, current_location, scale, streetview, navigation, coordinates, overview
		$mapContainer.data("mapControls", searchParams.get("Controls"));
	}

}


$(document).ready(function () {
	if (! document.createElementNS || ! document.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect) {
	  $("<div />")
			  .text(tr("Your browser does not support SVG (required for drawing), please upgrade."))
			  .addClass("browser-error")
			  .prependTo("#main")
			  .slideDown();
	}

	$(document).on("initialized", function() {	// map initialized event
		$(".olControlZoomBoxItemActive, .olControlZoomBoxItemInactive").attr("title", tr("Select Area to Zoom"));
		$(".overlay.top_icons a").each( function() {
			$(this).show();
		});
		var cookie = getCookie("hash", "maps");
		if (!location.hash && cookie) {
			cookie = cookie.replace(/^#/, "");
			location.hash = cookie;
			setTimeout( function () { $("img[alt=" + cookie + "]").parents(".anchor-toggle").click(); }, 2000);
			deleteCookie("hash", "maps");
		} else {

			// set the map layer if needed
			if (searchParams.has("Layer")) {
				$("select[name=baseLayers]").val(searchParams.get("Layer")).change();
			}
		}

		const map = $(".map-container:first")[0];

		const cgrafLayerRefresh = function( event ) {
			// all loaded and ready for mapping?
			let $optionalLayers = $(".optionalLayers");

			/*
			* needs layer=tracker_field_finder suffix=tracker_field_map_text params adding to all map plugins
			*
			* {searchlayer layer=tracker_field_finder suffix=tracker_field_map_text tracker_field_map_shareCode="{{shareCode}}" tracker_field_map="NOT {{mapId}}" refresh=60 maxRecords=2000 fields="tracker_field_poiDescription,tracker_field_zoneDescription" sort_mode="tracker_id_desc" popup_width="666" popup_height="600"}
			* */

			const selector = " > label:not(:first):not(.done)";
			let $theLabels = $(selector, $optionalLayers);

			// hide the first "Editable" one as it's not used any more
			$("> label:first:not(.d-none)", $optionalLayers).addClass("d-none");

			$theLabels.each(function() {
				const $this = $(this),
					label = $this.text(),
					parts = label.split(": ", 2),
					layer = $.trim(parts[0]);

				if (parts && parts.length === 2) {
					const layerClass = layer.replace(/[^\w]/g, "");
					let $child = null,
						$similar = $(selector + ":contains('" + layer + ": '), label.done." + layerClass, $optionalLayers);

					if ($optionalLayers.find("> ." + layerClass + "-container").length === 0 && $similar.length > 1) {
						$child = $("<div class='" + layerClass + "-container'>");
						const $clicker = $("<a>").text(layer);
						$clicker.append($().getIcon("caret-down")).click(function () {
							const $a = $(this),
								$myLabels = $a.nextAll("label");

							if ($myLabels.find(":visible").length) {
								$myLabels.hide("fast");
								$a.find(".icon").setIcon("caret-right");
							} else {
								$myLabels.show("fast");
								$a.find(".icon").setIcon("caret-down");
							}
						});
/* doesn't work, something to do with having detatched and moved the checkboxes i think...
						$child.append(
							$("<input type='checkbox' class='top-checkbox form-check-input' checked='checked'>").change(function () {
								const $this = $(this),
									checked = $this.is(":checked"),
									$myChecks = $this.parent().find("input[type=checkbox]:not(.top-checkbox)");

								$myChecks.each(function () {
									$(this).click();
								});
							})
						);
*/
						$child.append(" ", $clicker);
						$this.before($child);
					} else {
						$child = $optionalLayers.find("> div." + layerClass);
					}

					if ($child.length) {
						$similar.add($this);
						$similar.each(function () {
							const $doneLabel = $(this),
								$checkbox = $doneLabel.find("input[type=checkbox]").detach();

							$doneLabel
								.text($doneLabel.text().replace(layer + ": ", ""))
								.prepend($checkbox)
								.addClass("done ml-3 " + layerClass);

							$child.append($doneLabel);
						});
					}
				}
			});

/*
			// make zone layers z-index 0 and all others 1
			let map = $(".map-container:first")[0];
			$.each(map.map.layers, function (layerName, layer) {
				console.log(layer.name + " " + layer.getZIndex());
				if (layer.features && layer.CLASS_NAME === "OpenLayers.Layer.Vector") {
					layer.setZIndex(100);
					const layerDivId = layer.div.id;
					document.getElementById(layerDivId + "_root").style.zIndex = 100;
						layer.features.every(feature => {
						if (feature.geometry.CLASS_NAME === "OpenLayers.Geometry.Polygon") {
							console.log(feature.attributes.content + " " + feature.attributes.itemId);
							layer.setZIndex(1);
							document.getElementById(layerDivId + "_root").style.zIndex = 1;
							return false;	// stop
						}
						return true;
					});
				}
			});
*/

		};

		// tiki binds to the parent label for some reason
		const $toggleCheckbox = $("#toggleCheckbox");
		/* this also doesn't work as the "this" in the click event is detached
		$toggleCheckbox.parent().off("change");

		$toggleCheckbox.off("change").change(function () {
			const $container = $(this).closest(".anchor-content");
			let $checks = $("input[type=checkbox]:not(#toggleCheckbox)", $container);
			$checks.each(function () {
				$(this).click();
				$checks = $("input[type=checkbox]:not(#toggleCheckbox)", $container);
			});
		});*/

		$toggleCheckbox.parent().remove();

		map.map.events.register('addlayer', {}, cgrafLayerRefresh);
		map.map.events.register('removelayer', {}, cgrafLayerRefresh);
		map.map.events.register('changelayer', {}, cgrafLayerRefresh);
		map.map.events.register('changebaselayer', {}, cgrafLayerRefresh);

		return true;	// pass it on
	});

	$(document).on("iconsloaded", function( event ) {
		var $contentsDiv = $(".ui-dialog:last .contents");
		// get the first icon in the contents div, needed to be able to clone the click event
		var $exampleImg =  $("img:first", $contentsDiv).parent().clone(true);

		$("li > img", "#map-legend").each(function () {
			var $toAppend = $exampleImg.clone(true);
			var src = $(this).attr("src");
			var fileId = src.match(/\d*$/);	// ends in a number?
			if (fileId && fileId.length && fileId[0] && !isNaN(fileId[0])) {
				fileId = fileId[0];
				$("img", $toAppend).replaceWith($(this).clone());
				$toAppend.
					data("object", fileId).
					attr("href", src).
					attr("title", $(this).parent().text().replace($(this).parent().find("a").text(), ""));

				if ($("a[href='" + src + "']", $contentsDiv).length === 0) {
					$contentsDiv.append($toAppend);
				}
			}
		});
		$("image", ".map-container").each(function () {
			var $toAppend = $exampleImg.clone(true);
			//$("img", $toAppend).replaceWith($(this).clone());
			var src = $(this).attr("href");
			var fileId = src.match(/\d*$/);	// ends in a number?
			if (fileId && fileId.length && fileId[0] && !isNaN(fileId[0])) {
				$("img", $toAppend).attr("src", src);
				fileId = fileId[0];
				$toAppend.
					data("object", fileId).
					attr("href", src).
					attr("title", $(this).parent().text().replace($(this).parent().find("a").text(), ""));

				if ($("a[href='" + src + "']", $contentsDiv).length === 0) {
					$contentsDiv.append($toAppend);
				}
			}

		});
	});

	/// example code to allow line width to be modifiedExample
	$(".map-container").on("initialized", function () {
		// not ready for OpenLayers3+
		if (typeof ol === "undefined") {
			$(".map-container")[0].defaultStyleMap.styles.default.context.getStrokeWidth = function () {
				return 4;
			};
		}
	});

	$(".combine-maps").click(function () {

		let mapName = prompt(
			tr("Are you sure you want to duplicate this map and combine all features on maps with the same sharecode on to it?"),
			$("h3", ".map-info").text() + " (" + tr("combined") + ")"
		);

		if (! mapName) {
			return;
		}

		$("#appframe").tikiModal(tr("Combining Map and all shared features"));

		// TODO only duplicate selected layers?
		let layerCheckboxes = $("input[type=checkbox]", ".optionalLayers");

		// get all other maps with same sharecode then get all points and legends

		let mapTrackerId = $("input[name=trackerId]").val();

		$.getJSON("tiki-searchindex.php", {
			filter: {
				tracker_id: mapTrackerId,
				tracker_field_shareCode: $("input[name='filter~tracker_field_map_shareCode']").val()
			}
		}, function (data) {
			let mapId = $("input[name=itemId]").val(),
				otherMapIds = [], newMapId;

			$.each(data.result, function (k, entry) {
				if (entry.object_id !== mapId) {
					otherMapIds.push(entry.object_id);
				}
			});

			// duplicate this map and all features, and get the mapId

			$.post($.service("tracker", "clone_item"), {
					trackerId: mapTrackerId,
					itemId: mapId,
					fields: {
						mapName: mapName,
						shareCode: ""
					}
				}, function (data) {
					newMapId = data.created;

					// and duplicate points and legends with our newMapId

					let poiTrackerId = $("input[name=trackerId]", ".box-tracker_input").val(),
						legendTrackerId = $("#legend_tracker_id").val(),
						zoneTrackerId = $("input[name=trackerId]", ".box-map_edit_features").val();

					$.getJSON("tiki-searchindex.php", {
						filter: {
							//				POI trackerId form a hidden module
							tracker_id: poiTrackerId + " OR " + legendTrackerId + " OR " + zoneTrackerId,
							tracker_field_map: otherMapIds.join(" OR ")
						}
					}, function (data) {
						let numFeatures = data.result.length;

						$.each(data.result, function (k, entry) {
							let fields = {};

							if (entry.tracker_id === poiTrackerId) {
								fields = {
									poiDescription: entry.tracker_field_poiDescription + "\n\nOriginal owner: " + entry.tracker_field_finder,
								};
							} else if (entry.tracker_id === zoneTrackerId) {
								fields = {
									zoneDescription: entry.tracker_field_zoneDescription + "\n\nOriginal owner: " + entry.tracker_field_finder,
								}
							} else if (entry.tracker_id === legendTrackerId) {
								fields = {}
							} else {
								console.error("Error: entry.tracker_id not poi, zone or legend - " + entry.tracker_id);
							}

							fields.map = newMapId;
							fields.finder = jqueryTiki.username;

							$.post(
								{
									url: $.service("tracker", "clone_item"),
									async: false,						// blocking it - warning?
									dataType: "json",
									data: {
										trackerId: entry.tracker_id,
										itemId: entry.object_id,
										fields: fields,
									},
								},
							)
								.success(function (data) {
									// did it work?
									console.log(data);
								})
								.fail(function (xhr, status, message) {
									alert(tr("Something went wrong, sorry - " + message));
									console.error(message);
								})
								.always(function () {
									numFeatures--;
									if (!numFeatures) {
										$("#appframe").tikiModal();
										alert(tr("Map combined as - " + mapName));

										location.href = "./";

									}
								});
						});
					});
				},
				"json"
			);

		});

	});

	// remove the "Create another" checkbox from inset item forms
	//$("form[action=tiki-tracker-insert_item].simple").attr("action", "tiki-tracker-insert_item?modal=1");

	$(document).on( "dialogopen", ".ui-dialog", function( event, ui ) {
		let dialog = this;
		setTimeout(function () {
			$(dialog).find("input[name=next]").parents(".form-check").remove();

		}, 500);
	});

});



/***
 * Print out the current map and it's features
 */
function printMap(photos) {

	var printFeatureAttachments = !!photos;

	var container = $(".map-container", "#appframe")[0],
		$container = $(container),
		allDone = false,
		elements = [],
		mapId = $("input[name=itemId]").val() || $("input[name='forced~map']").val();

	while (container.map.controls.length) {
		container.map.controls[0].deactivate();
		container.map.removeControl(container.map.controls[0]);
	}

	$container.finalMapRefresh(function (result, feature) {
		if (feature.getVisibility()) {
			elements.push({
				data: result,
				feature: feature
			});
		}
	});


	var dialog = $("<div>" + tr("Make sure your page setup is set to landscape") + "</div>").dialog({
		modal: true,
		title: "Loading..."
	});

	setTimeout(function () {

		var item = [];

		$.ajax($.service("tracker", "view"), {	// get the full map item
			data: {
				id: mapId
			},
			async: false,
			type: "POST",
			dataType: "JSON"
		})
			.done(function (data) {
				item = data;
				var fields = item["fields"];
				for (var i = 0; i < fields.length; i++) {
					item[fields[i]["permName"]] = fields[i];
				}
			})
			.fail(function (jqXHR, textStatus) {
				console.log("Request failed: " + textStatus + " " + jqXHR.statusText);
				item = [];
			});

		var $legend = $("#map-legend"),
			$title = $("<h1/>").text($(".map-info h3").text()),
			$finder = $("<p/>").text(tr("By") + " " + $("input[name='forced~finder']", "#map-legend-add").val());
			//$date = $("<div/>").addClass("date").text(item.modification_date.substr(0,10));

		var $main = $("body > .container:first");
		$main
			.empty()
			.empty()
			.append($title)
			.append($finder)
			//.append($date)	// date not available in trascker view yet
			.append(container)
			.addClass("printing");

		// try and work out the max height for the main map
		var offset = $title.outerHeight() + $finder.outerHeight(); // + $date.outerHeight(),
			height = $container.height(),
			max = 600;	// seems somewhat arbitrary nothing else seems to do it

		if (height > max) {
			$container.height(max - offset);
		}
		container.map.updateSize();

		// add description here
		$("<div />")
			.addClass("map-description")
//			.height($container.height())
			.append($title.clone())
			.append($finder.clone())
			.append($("<div />").html(item["mapDescription"]["pvalue"]))
			.appendTo($main);

		$legend.appendTo($main);

		var mapCount = elements.length + 1;

		var finishPrinting = function () {	// called after the print dialog clears
			setTimeout(function () {
				alert(tr("Click ok to continue"));
				if (window.location.hash) {
					window.location.replace(window.location.href.replace(window.location.hash, "#" + tr("Share")));
				} else {
					window.location.replace(window.location.href + "#" + tr("Share"));
				}
				window.location.reload(true);
			}, 1000);
		};

		$.each(elements, function (k, data) {

			var div = $("<div/>").addClass("map-feature");

			var div2 = 	$("<div />").addClass("map-description").css("page-break-before", "avoid");

			var $fTitle = $("<h2>")
				.append($("<img>").attr("src", data.data.tracker_field_poiIcon))
				.append($(data.data.link).text())
				.appendTo(div2);

			var $fFinder = $("<div/>")
				.addClass("finder")
				.append(tr("By") + " " + data.data.tracker_field_finder)
				.appendTo(div2);

			var $fDate = $("<div/>")
				.addClass("date")
				.text(data.data.modification_date.substr(0,10))
				.appendTo(div2);

			$("<div/>")
				.addClass("description")
				.addClass("clearfix")
				.append(data.data.tracker_field_zoneDescription)
				.append(data.data.tracker_field_poiDescription)
				.appendTo(div2);


			var image = data.data.tracker_field_poiImage || data.data.tracker_field_zoneImage,
				i = 0, images = [],  $img;

			if (image) {
				images = image.split(",");

				$.ajax('tiki-searchindex.php', {
					data: {
						filter: {
							object_type: "file",
							object_id: images.join(" OR ")
						}
					},
					async: false,
					type: "POST",
					dataType: "JSON"
				})
					.done(function (data) {
						images = data.result;
					})
					.fail(function (data) {
						if (data.result) {
							images = data.result;
						} else {
							images = [];
						}
					});

				for (i in images) {
					if (images[i].filetype === "image/svg") {
						$img = $("<object type=\"image/svg+xml\" data=\"display" + images[i].object_id + "?y=120\" height=\"120\">");
					} else {
						$img = $("<img src=\"display" + images[i].object_id + "?y=120\" height=\"120\">");
					}
					$("<div/>")
						.addClass("poiImage")
						.append($img)
						.appendTo(div2);
				}
			}

			div2.appendTo(div);

			$("<div>").width(200).height(200)
				.appendTo(div)
				.data("map-controls", "")
				.createMap()
				.bind("initialized", function () {
					this.vectors.addFeatures([data.feature.clone()]);
					this.map.zoomToExtent(data.feature.geometry.getBounds());

					if (container.map.getZoom() < this.map.getZoom()) {
						this.map.zoomTo(container.map.getZoom());
					}

					if (--mapCount === 0) {
						setTimeout(function () {
							dialog.dialog("destroy");
							window.print();
							finishPrinting();
						}, printFeatureAttachments ? 4000 : 500);
					}
				});


			if (images && printFeatureAttachments) {
				for (i in images) {

					if (images[i].filetype === "image/svg") {
						$img = $("<object type=\"image/svg+xml\" data=\"display" + images[i].object_id + "?y=500\" height=\"500\">");
					} else {
						$img = $("<img src=\"display" + images[i].object_id + "?y=500\" height=\"500\">");
					}
					$("<div/>")
						.addClass("map-description big-image")
						.append($fTitle.clone())
						.append($("<h4>").text(images[i].title))
						.append($img)
						.append($("<div>").addClass("description").html(images[i].description))
						.appendTo(div);

					$("<div>").width(100).height(100)
						.appendTo(div)
						.data("map-controls", "")
						.createMap()
						.bind("initialized", function () {
							this.vectors.addFeatures([data.feature.clone()]);
							this.map.zoomToExtent(data.feature.geometry.getBounds());

							if (container.map.getZoom() < this.map.getZoom()) {
								this.map.zoomTo(container.map.getZoom());
							}
						});
				}
			}

			$("body > .container:first").append(div);
		});

		if (--mapCount === 0) {
			setTimeout(function () {
				dialog.dialog("destroy");
				window.print();
				finishPrinting();
			}, printFeatureAttachments ? 4000 : 500);
		}

	}, 2000);

}


