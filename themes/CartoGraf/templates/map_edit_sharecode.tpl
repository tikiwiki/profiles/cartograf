{if $input.mapId}
	<form id="edit-share-code" method="post" action="{service controller=tracker action=update_item itemId=$input.mapId trackerId=$input.trackerId}">
		<label class="form-group w-100">
			{tr}Share Code{/tr}
			<input type="text" name="fields~shareCode" value="{$input.shareCode|escape}" id="sharecode" style="width:80px" class="form-control">
			<img src="img/icons/error.png" style="display:none" width="16" height="16" title="{tr}Share{/tr}|{tr}Share codes can contain only letters and numbers{/tr} {if $prefs.unified_engine eq 'mysql'}and a minimum length of five characters{/if}" class="tips" />
		</label>
		<input type="submit" value="{tr}Modify{/tr}"/>
		{if not empty($input.shareCode)}
			<label class="form-group w-100">
				<input type="checkbox" id="join" onchange="$('#appframe .map-container').trigger('initialized');">
				{tr}Allow Collaboration{/tr}
			</label>
			<label>
				<input type="checkbox" id="dupe" onchange="$('#appframe .map-container').trigger('initialized');">
				{tr}Allow Duplication{/tr}
			</label>
		{/if}
	</form>
{/if}
{if not empty($input.shareCode) or empty($input.mapId)}
	<form id="show-share-code" class="simple" method="POST" action="{$page|escape}" style="width:100%; margin-top: 4px;">
		<label class="form-group w-100">
			<span id="permalinkLabel">{tr}Public Permalink:{/tr}</span>
			<input id="permalink" type="url" value="" class="form-control">
		</label>
		<label class="form-group w-100">
			{tr}Embed Code:{/tr}
			<textarea id="embed-map" class="form-control"></textarea>
		</label>
	</form>
{/if}
{if $prefs.sefurl_short_url eq 'y'}
	{module module='short_url' nobox='y'}
{/if}
{jq}
	var input = {{$input|json_encode}}
		, baseUrl = {{$base_url|json_encode}}
		, page = {{$page|json_encode}}
		;
	$('#edit-share-code').submit(function () {
		if ($("#sharecode").val().match(/\W/)) {
			alert("{tr}Share codes can contain only letters and numbers{/tr}");
			$("#sharecode").focus().keyup();
			return false;
		}
{{if $prefs.unified_engine eq 'mysql'}
		if ($("#sharecode").val() && $("#sharecode").val().length < 5) {
			alert("{tr}Share codes must be at least five characters long{/tr}");
			$("#sharecode").focus().keyup();
			return false;
		}
{/if}}
		var form = this;

		$.post($(this).attr('action'), $(this).serialize(), function () {
			document.location.href = 
				page + '?coordinates=' + encodeURIComponent(input.coordinates)
				+ '&mapId=' + encodeURIComponent(input.mapId)
				+ '&mapName=' + encodeURIComponent(input.mapName)
				+ '&shareCode=' + encodeURIComponent($(':text', form).val())
				+ '#Share'
				;
		}, 'json');
		return false;
	});
	$("#sharecode").keyup(function(){
		if ($(this).val().match(/\W/)) {
			$(this).next("img").show();
		} else {
			$(this).next("img").hide();
		}
	});
	$('#appframe .map-container').bind('initialized', function () {
		var container = this, updatePermalink;
		updatePermalink = function () {
			var url = baseUrl + page + '?coordinates=' + encodeURIComponent($(container).getMapCenter()), shareUrl = '',
				join = $("#join:checked").length, dupe = $("#dupe:checked").length;

			if (input.shareCode) {
				url = url
					+ '&shareCode=' + encodeURIComponent(input.shareCode)
					+ '&mapName=' + encodeURIComponent(input.mapName)
					+ '&sourceId=' + encodeURIComponent(input.mapId ? input.mapId : input.sourceId)
					;
				shareUrl = url
					+ (join ? '&join=y' : '')
					+ (dupe ? '&dupe=y' : '')
					+ '#' + tr('Map')
					;
			}
			if (join && dupe) {
				$('#permalinkLabel').text("{tr}Link allowing duplication & collaboration:{/tr}");
			} else if (join) {
				$('#permalinkLabel').text("{tr}Link to allow collaboration:{/tr}");
			} else if (dupe) {
				$('#permalinkLabel').text("{tr}Link to allow duplication:{/tr}");
			} else {
				$('#permalinkLabel').text("{tr}Public Permalink:{/tr}");
			}
			$('#permalink').parent().attr("title", $('#permalink').val());

			$('#permalink')
				.val(shareUrl)
				.closest('form').attr('action', shareUrl);
			$('#embed-map')
				.val($('<iframe/>').attr('src', url).attr('height', 400).attr('width', 600).wrap('<div/>').parent().html());
		};

		// not ready for OpenLayers3+
		if (typeof ol === "undefined") {
			this.map.events.on({
				move: updatePermalink
			});

			updatePermalink();
		}
	});
	$('#permalink, #embed-map').click(function () {
		$(this).select();
		return false;
	});
	// fix for removing scrollbars on maps only
	$("body").css("overflow", "hidden");

	$(document).on( "dialogopen", function( event, ui ) {
		$("body").css("overflow", "visible");
	});
	$(document).on( "dialogclose", function( event, ui ) {
		$("body").css("overflow", "hidden");
	});
{/jq}
