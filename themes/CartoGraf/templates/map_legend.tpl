
<form id="map-legend" method="get" action="tiki-searchindex.php">
	<h3>{tr}Legend{/tr}</h3>

	<input type="hidden" name="filter~tracker_id" value="{$input.trackerId|escape}" id="legend_tracker_id">
	<input type="hidden" name="sort_mode" value="title_asc"/>

	{if $input.shareCode}
		<input type="hidden" name="filter~tracker_field_map_shareCode" value="{$input.shareCode|escape}"/>
	{else}
		<input type="hidden" name="filter~tracker_field_map" value="{$input.mapId|escape}"/>
	{/if}
	<ul>
	</ul>
</form>
{if $user ne ""}
	{capture assign='owner'}{strip}{wikiplugin _name='list'}{literal}
		{pagination max=1}
		{filter type="trackeritem"}
		{filter field="object_id" content="{/literal}{$input.mapId|escape}{literal}"}
		{filter field="tracker_field_owner" content="{/literal}{$user}{literal}"}
		{OUTPUT()}1{OUTPUT}
		{FORMAT(name="country")}{display name="tracker_field_countryCountry" format="trackerrender" default=""}{FORMAT}
		{ALTERNATE()}0{ALTERNATE}
	{/literal}{/wikiplugin}{/strip}{/capture}
	{if $owner}
		<form id="map-legend-add" method="post" action="{service controller=tracker action=insert_item trackerId=$input.trackerId}">
			<input type="hidden" name="trackerId" value="{$input.trackerId|escape}"/>
			<input type="hidden" name="forced~finder" value="{$user|escape}"/>
			<input type="hidden" name="forced~map" value="{$input.mapId|escape}"/>
			<input type="submit" value="{tr}New Entry{/tr}"/>
		</form>
	{/if}
{/if}
{jq}
	var mapId = {{$input.mapId|json_encode}};
	var user = "{{$user}}";
	function refresh_legend() {
		var $form = $('#map-legend');
		$.getJSON($form.attr('action'), $form.serialize())
			.success(function (data) {
				var ul = $form.find('ul').empty();
				$.each(data.result, function (k, entry) {
					entry.tracker_field_legendIcon += (entry.tracker_field_legendIcon.indexOf("?") < 0 ? "?" : "&") + "display";
					var item = $('<li/>')
						.text(entry.title)
						.prepend($('<img/>').attr('src', entry.tracker_field_legendIcon))
						.appendTo(ul);

					if (mapId == entry.tracker_field_map && user && user == entry.tracker_field_finder) {
						item.append($('<a class="editlink"/>')
							.text(tr('Modify'))
							.attr('href', $.service('tracker', 'update_item', {
								trackerId: entry.tracker_id,
								itemId: entry.object_id
							})));
						item.append($('<a class="deletelink"/>')
							.text(tr('Delete'))
							.attr('href', $.service('tracker', 'remove_item', {
								trackerId: entry.tracker_id,
								itemId: entry.object_id
							})));

						item.find('a')
							.click(function () {
								$(this).serviceDialog({
									title: $(this).text(),
									load: function () {
										$("h1:first").remove();	// remove unnecessary title
									},
									success: refresh_legend
								});

								return false;
							});
					}
				});
			});
	}

	refresh_legend();
	setInterval(refresh_legend, 120*1000);

	$('#map-legend-add').submit(function () {
		$(this).serviceDialog({
			title: $(':submit', this).val(),
			load: function () {
				$("h1:first").remove();	// remove unnecessary title
				$(".checkbox:last").remove(); // remove the "Create another" checkbox
			},
			controller: 'tracker',
			action: 'insert_item',
			data: $(this).serialize(),
			success: refresh_legend
		});
		return false;
	});
{/jq}
