<div class="map-info">
	<h3>{$input.title|escape}</h3>
	<p class="clearfix">
		{if $input.mapId}
			<a class="service-dialog-view floatleft" href="{service controller=object action=infobox type=trackeritem object=$input.mapId mode=divs}">{tr}Description{/tr}</a>
		{elseif $input.sourceId}
			<a class="service-dialog-view floatleft" href="{service controller=object action=infobox type=trackeritem object=$input.sourceId mode=divs}">{tr}Description{/tr}</a>
		{/if}
		<a class="mymaps floatright" href="{tr}HomePage{/tr}">{tr}My Maps{/tr}</a>
	</p>
</div>
{jq}
	var page = {{$page|json_encode}};
	$('.map-info .service-dialog-view').click(function () {
		let $form;
		$(this).serviceDialog({
			title: $(this).text(),
			load: function () {
				$("h1:first a", this).parent().html($("h1:first a", this).text());	// remove a tag on title
				$("a.service-dialog:eq(1)", this).remove();							// and loose the confusing delete button
				$('.service-dialog').click(function () {
					$(this).serviceDialog({
						title: $(this).text(),
						load: function () {
							$("form", this).submit(function () {
								$form = $(this).tikiModal("Saving...");
							}).removeClass("confirm-action");
						},
						success: function (data) {
							if (data.fields) {	// service no longer returns the fields (meh) so capture them in the submit handler

								var url = page + "?mapId=@itemId@&coordinates=@mapBaseLocation@&shareCode=@shareCode@&mapName=@mapName@#Map"
									.replace("@itemId@", encodeURIComponent(data.itemId));

								$.each(data.fields, function (key, value) {
									url = url.replace("@" + key + "@", encodeURIComponent(value));
								});
							} else if (coords) {
								url = document.location.href.replace(/coordinates=[^&]+/, "coordinates=" + coords).replace(/mapName=[^&#]+/, "mapName=" + mapName);
							} else {	// delete also returns via here but with only itemId and trackerId
								url = "./";
							}
							$form.tikiModal();
							location.replace(url);
						}
					})
					return false;
				});
			}
		});
		return false;
	});

	var id = {{if $input.mapId}{$input.mapId}{elseif $input.sourceId}{$input.sourceId}{/if}};
	if (! location.hash.match(/(:?EmbedMode|NoDescription)/) && ! getCookie(id, "maps")) {
		setCookie(id, 1, "maps");
		setTimeout(function(){$('.map-info .service-dialog-view').click();}, 3000);
	}
{/jq}
