<div class="map-collaborate{permission type=tracker object=$input.trackerId name='create_tracker_items'} floatright{/permission}" style="line-height:24px">
	{if $input.sourceId and not empty($smarty.get.join) and not $smarty.get.join neq 'n'}
		<a class="service-dialog-edit" href="#">{tr}Collaborate{/tr}</a>
	{/if}
</div>
{jq}
	var permOk = false, page = {{$page|json_encode}};
	{{permission type=tracker object=$input.trackerId name='create_tracker_items'}}
		permOk = true;
	{{/permission}}
	$('.map-collaborate .service-dialog-edit').click(function () {
		if (!permOk) {
			if (confirm(tr("You need to log in to collaborate on this map, do you want to log in now?"))) {
				setCookie("hash", location.hash, "maps");
				document.location.href = "tiki-login_scr.php";
			}
		} else {
			$clicked = $(this);
			$clicked.parent().tikiModal(" ");
			$.getJSON('tiki-searchindex.php', {
					filter: {
						object_type:"trackeritem",
						object_id:{{$input.sourceId}}
					},
					fields:["tracker_field_mapName","tracker_field_mapDescription","tracker_field_shareCode","tracker_field_mapDetailsHeader","tracker_field_mapBaseLocation","tracker_field_owner"]
				}, function (data) {
					if (!!data) {
						// now take the results data, create a fake anchor and trigger the insert_item service dialog (must be an easier way, no?)
						var result = data.result[0];
						$.getJSON($.service('tracker', 'list_fields'), {
							trackerId: {{$input.trackerId}}
						}, function (data) {
							// need the ins_ id's from the fields
							var fields = data.fields;
							var fld, input = { status: result.tracker_status, forced: {} };
							for (fld in fields) {
								switch(fields[fld].permName) {
									case "owner":
										input.forced[fields[fld].permName] = "{{$input.owner}}";
										break;
									case "shareCode":
										input.forced[fields[fld].permName] = result.tracker_field_shareCode;
										break;
									case "mapName":
										input["ins_" + fields[fld].fieldId] = result.tracker_field_mapName + " " + "{tr}with{/tr}" + " {{$input.owner}} " + "{tr}as a collaborator{/tr}";
										break;
									case "mapDescription":
										input["ins_" + fields[fld].fieldId] = result.tracker_field_mapDescription;
										break;
									case "mapBaseLocation":
										input.forced[fields[fld].permName] = result.tracker_field_mapBaseLocation;
										break;
									case "mapDetailsHeader":
										input.forced[fields[fld].permName] = result.tracker_field_mapDetailsHeader;
										break;
									default:
										break;
								}

							}
							$clicked.parent().tikiModal();
							$clicked.parent().append($('<a style="display:none" href="{{service controller=tracker action=insert_item trackerId=$input.trackerId}}">{{tr}}Collaborate{{/tr}}</a>').
								serviceDialog({
									title: $(this).text(),
									data: input,
									success: function (data) {
										var url = page + "?mapId=@itemId@&coordinates=@mapBaseLocation@&shareCode=@shareCode@&mapName=@mapName@"
											.replace("@itemId@", encodeURIComponent(data.itemId));

										$.each(data.fields, function (k, v) {
											url = url.replace("@" + k + "@", encodeURIComponent(v));
										});

										document.location.href = url + "#Map";
									}
								}).click()
							);
						});
					}
				}
			);
		}
		return false;
	});
{/jq}
