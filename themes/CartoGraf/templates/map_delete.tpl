{permission type='trackeritem' object=$input.mapId name='remove_tracker_items'}
{if $user}
<form id="delete-form" class="simple floatright" method="post" action="{service controller=tracker action=remove_item}">
	<div class="submit">
		<input type="hidden" name="trackerId" value="{$input.trackerId|escape}"/>
		<input type="hidden" name="itemId" value="{$input.mapId|escape}"/>
		<input type="submit" value="{tr}Delete{/tr}"/>
	</div>
</form>
{jq}
$('#delete-form').submit(function () {
	if (confirm(tr("Are you sure you want to permanently remove this map and all the data associated?"))) {
		$.tikiModal("{tr}Deleting...{/tr}");
		$.post($(this).attr('action'), $(this).serialize(), function (data) {
			location.replace("./");
		}, 'json');
	}
	return false;
});
{/jq}
{/if}
{/permission}
&nbsp;
{if not empty($smarty.get.shareCode) and ($tiki_p_admin eq 'y' or 'TeachersPowerUsers'|in_group)}
	<div class="clearfix" style="margin-top:2rem;margin-left:1rem;">
		<a class="mymaps floatleft combine-maps tips" href="#"
		   title="|{tr}Combines the selected layers into one new map{/tr}">{tr}Combine Maps{/tr}</a>
	</div>
{/if}

