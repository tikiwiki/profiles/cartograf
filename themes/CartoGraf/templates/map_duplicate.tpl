{if not empty($smarty.get.mapId) or (not empty($smarty.get.dupe) and not $smarty.get.dupe neq 'n')}
	<form id="duplicate-form" class="simple center" method="post" action="{service controller=tracker action=clone_item}">
		<div class="submit">
			<input type="hidden" name="trackerId" value="{$input.trackerId|escape}"/>
			<input type="hidden" name="itemId" value="{$input.mapId|escape}"/>
			<input type="hidden" name="fields~shareCode" value=""/>
			<input type="hidden" name="fields~mapBaseLocation" value="{$input.coordinates|escape}"/>
			<input type="hidden" name="fields~mapName" value="{$input.mapName|cat:' '|cat:'{tr}copy{/tr}'|escape}"/>
			<input type="hidden" name="fields~owner" value="{{$user}}"/>
			<input type="submit" value="{tr}Duplicate{/tr}"/>
		</div>
	</form>

	{jq}
var permOk = false;
{{permission type=tracker object=$input.trackerId name='create_tracker_items'}}
	permOk = true;
{{/permission}}
$('#duplicate-form').submit(function () {
	if (!permOk) {
		if (confirm(tr("You need to log in to duplicate this map, do you want to log in now?"))) {
			setCookie("hash", location.hash, "maps");
			document.location.href = "tiki-login_scr.php";
		}
	} else {
		var newName = prompt(tr("Enter new map name"), $("input[name='fields~mapName']").val()),
				page = {{$page|json_encode}};
		if (newName) {
			$.tikiModal("{tr}Duplicating...{/tr}");
			$("input[name='fields~mapName']").val(newName);
			$.post($(this).attr('action'), $(this).serialize(), function (data) {
				document.location.href = page + '?mapId=' + data.created + '&mapName=' + encodeURIComponent(data.data.mapName) + '&coordinates=' + encodeURIComponent(data.data.mapBaseLocation) + "#{tr}Map{/tr}";
			}, 'json');
		}
	}
	return false;
});
	{/jq}
{/if}
<div id="toggleCheckBoxHolder" style="display: none;">
	<label style="font-weight:normal">&nbsp;<input type="checkbox" id="toggleCheckbox" checked="checked">&nbsp;{tr}Toggle{/tr}</label>
</div>
{jq}$(".box-map_layer_selector").parent().prev("h4").append($("#toggleCheckbox").parent());
$("#toggleCheckbox").parent().change(function () {
	var $checks = $("input[type=checkbox]", ".box-map_layer_selector");
	for (var i=0; i < $checks.length; i++) {
		$($checks[i]).click();
		$checks = $("input[type=checkbox]", ".box-map_layer_selector");		// changing layers empties and reloads this selector in the refreshLayers fn
	}
});
{/jq}
